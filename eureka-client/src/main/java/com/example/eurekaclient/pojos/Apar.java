
/**   
 * 	This is an entity class for defining details of a transaction 
 */
package com.example.eurekaclient.pojos;

import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;


/**
 * This is an entity class for defining details of a transaction
 * 
 * @author schennapragada
 *
 */
//@Entity
//@Table(name = "Apar")
public class Apar {

	/**
	 * Transaction ID is the primary key and is a Generated value
	 */
//	@Id
//	@GeneratedValue
//	@Column(name = "transactionid", nullable = false)
	private UUID transactionID;
//	@Column(name = "created_by", nullable = false)
	private String createdBy;
//	@Column(name = "created_time", nullable = false)
	private java.time.LocalDate createdTime;
//	@Column(name = "transaction_amount", nullable = false)
	private BigDecimal transactionAmount;
//	@Column(name = "transaction_date", nullable = false)
	private Instant transactionDate;
//	@Column(name = "transaction_type", nullable = false)
	private String transactionType;
//	@Column(name = "updated_by", nullable = false)
	private String updatedBy;
//	@Column(name = "updated_time", nullable = false)
	private java.time.LocalDate updatedTime;

	/**
	 * Creates a many to one mapping between this column and the primary key in
	 * Category table
	 */
//	@ManyToOne
//	@JoinColumn(name = "categoryid")
	private Category categoryID;

	/**
	 * Creates a many to one mapping between this column and the primary key in
	 * AccountDetails table
	 */
//	@ManyToOne
//	@JoinColumn(name = "from_accountid")
	private AccountDetails fromAccountID;

	/**
	 * Creates a many to one mapping between this column and the primary key in
	 * AccountDetails table
	 */
//	@ManyToOne
//	@JoinColumn(name = "to_accountid")
	private AccountDetails toAccountID;

	public Apar() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Apar(UUID transactionID, String createdBy, BigDecimal transactionAmount, String transactionType,
			String updatedBy, Category categoryid, AccountDetails fromAccountID, AccountDetails toAccountID) {
		super();
		this.transactionID = transactionID;
		this.createdBy = createdBy;
//		this.createdTime = createdTime;
		this.transactionAmount = transactionAmount;
//		this.transactionDate = transactionDate;
		this.transactionType = transactionType;
		this.updatedBy = updatedBy;
//		this.updatedTime = updatedTime;
		this.categoryID = categoryid;
		this.fromAccountID = fromAccountID;
		this.toAccountID = toAccountID;
	}

	/**
	 * Getters and Setter methods for Apar Class
	 * 
	 */

	public UUID getTransactionID() {
		return transactionID;
	}

	public void setTransactionID(UUID transactionID) {
		this.transactionID = transactionID;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public java.time.LocalDate getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(java.time.LocalDate createdTime) {
		this.createdTime = createdTime;
	}

	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public Instant getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Instant transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public java.time.LocalDate getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(java.time.LocalDate updatedTime) {
		this.updatedTime = updatedTime;
	}

	public Category getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(Category categoryID) {
		this.categoryID = categoryID;
	}

	public AccountDetails getFromAccountID() {
		return fromAccountID;
	}

	public void setFromAccountID(AccountDetails fromAccountID) {
		this.fromAccountID = fromAccountID;
	}

	public AccountDetails getToAccountID() {
		return toAccountID;
	}

	public void setToAccountID(AccountDetails toAccountID) {
		this.toAccountID = toAccountID;
	}

}
