package com.example.eurekaclient.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.eurekaclient.pojos.Forecast;


@RestController
public class EurekaClientRestController {
	
	private static final Logger logger = LoggerFactory.getLogger(EurekaClientRestController.class);

	@Autowired
	private DiscoveryClient discoveryClient;

	@Autowired
	RestTemplate restTemplate;
	
	@RequestMapping("/service-instances/{applicationName}")
	public List<ServiceInstance> serviceInstancesByApplicationName(@PathVariable String applicationName) {
		return this.discoveryClient.getInstances(applicationName);
	}
	
	private static String url = "http://MaruthiForecast";
	
	@GetMapping("/")
	public String greeting()
	{
		logger.info("publish the greeting");
		return "Hello there!";
	}
	
	@GetMapping("/allforecasts")
	public Iterable<Object> getAllForecasts() {
		logger.info("getting all forecasts");;
		Object[] iterable = restTemplate.getForObject(url + "/allforecasts", Object[].class);
		logger.info("returned the array");
		return Arrays.asList(iterable);
	}
	
	@GetMapping("/forecast/{forecastID}")
	public ResponseEntity<Forecast> getForecastID(@PathVariable(value = "forecastID") UUID forecastID) {
		logger.info("getting a forecast based on id");
		ResponseEntity<Forecast> forecast = restTemplate.getForEntity(url + "/forecast/{forecastID}", Forecast.class, forecastID);
		logger.info("returned the object");
		return forecast;
	}
	
	@GetMapping("/getbyrole/{roleid}/{visibility}")
	public Iterable<Object> findForecastsForRole(@PathVariable(value = "roleid") UUID roleID,
			@PathVariable(value = "visibility") String visibility) {
		logger.info("getting by role and visibility");
		Object[] iterable = restTemplate.getForObject(url + "/getbyrole/{roleid}/{visibility}", Object[].class, roleID, visibility);
		logger.info("returned the array");
		return Arrays.asList(iterable);
	}
	
	@PostMapping("/newforecast")
	public Forecast createNewforecast(@Validated @RequestBody Forecast forecast) {
		logger.info("sending to demo app");
		Forecast result = restTemplate.postForObject(url + "/newforecast", forecast, Forecast.class);
		logger.info("returned the saved forecast");
		return result;
	}

	/*
	 * @PutMapping("/updateforecast/{forecastID}") public Forecast
	 * updateForecastDetails(@PathVariable(value = "forecastID") UUID forecastID,
	 * 
	 * @Validated @RequestBody Forecast forecast) { ResponseEntity<Forecast>
	 * oldforecast = restTemplate.getForEntity(url + "/forecast/{forecastID}",
	 * Forecast.class, forecastID); Map<String, String> params = new HashMap<String,
	 * String>(); params.put(oldforecast.getBody().getForecastID().toString(),
	 * forecast.getForecastID().toString());
	 * params.put(oldforecast.getBody().getForecastName(),
	 * forecast.getForecastName()); //
	 * params.put(oldforecast.getBody().getForecastDescription(),
	 * forecast.getForecastDescription()); //
	 * params.put(oldforecast.getBody().getVisibility(), forecast.getVisibility());
	 * // params.put(oldforecast.getBody().getStartDate().toString(),
	 * LocalDate.now().toString()); // params.put("7","7"); //
	 * params.put(oldforecast.getBody().getCreatedBy(), forecast.getCreatedBy()); //
	 * params.put(oldforecast.getBody().getUpdatedBy(), forecast.getUpdatedBy()); //
	 * params.put(oldforecast.getBody().getAccountID().toString(),
	 * forecast.getAccountID().toString()); //
	 * params.put(oldforecast.getBody().getRoleID().toString(),
	 * forecast.getRoleID().toString());
	 * 
	 * // Map<String, String> params = new HashMap<String, String>(); //
	 * params.put("forecastName", forecast.getForecastName()); //
	 * params.put("forecastDescription", forecast.getForecastDescription()); //
	 * restTemplate.pu return forecast; }
	 */
	
	@DeleteMapping("/deleteforecast/{visibility}/{forecastName}")
	public Map<String, Boolean> deleteForecast(@PathVariable(value = "visibility") String visibility,
			@PathVariable(value = "forecastName") String forecastName) {
		logger.info("deleting the forecast");
		restTemplate.delete(url + "/deleteforecast/{visibility}/{forecastName}", visibility, forecastName);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
	
}
