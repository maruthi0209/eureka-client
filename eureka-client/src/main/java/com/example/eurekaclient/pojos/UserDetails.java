
package com.example.eurekaclient.pojos;

import java.util.UUID;


/**
 * An Entity for defining details about userdetails
 * 
 * @author schennapragada
 *
 */
//@Entity
//@Table(name = "UserDetails")
public class UserDetails {

	/**
	 * UserID is primary key and is a Generated value
	 */
//	@Id
//	@GeneratedValue
//	@Column(name = "userid", nullable = false)
	private UUID userID;
//	@Column(name = "user_name")
	private String userName;

//	@ManyToOne
//	@JoinColumn(name = "roleid")
	private UserRole roleID;

	public UserDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserDetails(UUID userID, String userName, UserRole roleID) {
		super();
		this.userID = userID;
		this.userName = userName;
		this.roleID = roleID;
	}

	/**
	 * Getters and Setters for this class
	 * 
	 */

	public UUID getUserID() {
		return userID;
	}

	public void setUserID(UUID userID) {
		this.userID = userID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public UserRole getRoleID() {
		return roleID;
	}

	public void setRoleID(UserRole roleID) {
		this.roleID = roleID;
	}

	/**
	 *  To String method
	 */
	@Override
	public String toString() {
		return "UserDetails [userID=" + userID + ", userName=" + userName + "]";
	}

}
