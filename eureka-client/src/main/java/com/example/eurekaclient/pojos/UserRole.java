package com.example.eurekaclient.pojos;

import java.util.UUID;


/**
 * An Entity for defining details about userroles
 * 
 * @author schennapragada
 *
 */
//@Entity
//@Table(name = "UserRole")
public class UserRole {

	/**
	 * UserID is primary key and is a Generated value
	 */
//	@Id
//	@GeneratedValue
//	@Column(name = "roleid", nullable = false)
	private UUID roleID;
//	@Column(name = "role_description", nullable = false)
	private String roleDescription;

	public UserRole() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserRole(UUID roleID, String roleDescription) {
		super();
		this.roleID = roleID;
		this.roleDescription = roleDescription;
	}

	/**
	 * Getters and Setters for this class
	 * 
	 */

	public UUID getRoleID() {
		return roleID;
	}

	public void setRoleID(UUID roleID) {
		this.roleID = roleID;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	/**
	 *  To String method
	 */
	@Override
	public String toString() {
		return "UserRole [roleID=" + roleID + ", roleDescription=" + roleDescription + "]";
	}

}
