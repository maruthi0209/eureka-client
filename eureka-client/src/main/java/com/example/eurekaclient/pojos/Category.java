
package com.example.eurekaclient.pojos;

import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.UUID;


/**
 * This is an entity class for defining details of an category
 */

//@Entity
//@Table(name = "Category")
public class Category {

	/**
	 * categoryID is the Primary key for this entity and is uniquely generated
	 */
//	@Id
//	@GeneratedValue
//	@Column(name = "categoryid", nullable = false)
	private UUID categoryID;
//	@Column(name = "category_type", nullable = false)
	private String categoryType;
//	@Column(name = "category_name", nullable = false)
	private String categoryName;
//	@Column(name = "category_amount", nullable = false)
	private BigDecimal categoryAmount;
//	@Column(name = "created_by", nullable = false)
	private String createdBy;
//	@Column(name = "created_time", nullable = false)
	private java.time.LocalDate createdTime;
//	@Column(name = "updated_by", nullable = false)
	private String updatedBy;
//	@Column(name = "updated_time", nullable = false)
	private java.time.LocalDate updatedTime;

	/**
	 * Creates a many to one mapping between this column and the primary key in
	 * Forecast table
	 */
//	@ManyToOne
//	@JoinColumn(name = "forecastid", nullable = false)
	private Forecast forecastID;

	public Category() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Category(UUID categoryID, String categoryType, String categoryName, BigDecimal categoryAmount,
			String createdBy, String updatedBy, Forecast forecastID) {
		super();
		this.categoryID = categoryID;
		this.categoryType = categoryType;
		this.categoryName = categoryName;
		this.categoryAmount = categoryAmount;
		this.createdBy = createdBy;
//		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
//		this.updatedTime = updatedTime;
		this.forecastID = forecastID;
	}

	/**
	 * Getter and Setter methods for Category class
	 * 
	 */

	public UUID getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(UUID categoryID) {
		this.categoryID = categoryID;
	}

	public String getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public BigDecimal getCategoryAmount() {
		return categoryAmount;
	}

	public void setCategoryAmount(BigDecimal categoryAmount) {
		this.categoryAmount = categoryAmount;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDate getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(LocalDate createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public LocalDate getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(LocalDate updatedTime) {
		this.updatedTime = updatedTime;
	}

	public Forecast getForecastID() {
		return forecastID;
	}

	public void setForecastID(Forecast forecastID) {
		this.forecastID = forecastID;
	}

	/**
	 * To string method for this class
	 */
	@Override
	public String toString() {
		return "Category [categoryID=" + categoryID + ", categoryType=" + categoryType + ", categoryName="
				+ categoryName + ", categoryAmount=" + categoryAmount + ", createdBy=" + createdBy + ", createdTime="
				+ createdTime + ", updatedBy=" + updatedBy + ", updatedTime=" + updatedTime + ", forecastID="
				+ forecastID + "]";
	}

}
