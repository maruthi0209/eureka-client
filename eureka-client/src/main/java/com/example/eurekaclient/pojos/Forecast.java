package com.example.eurekaclient.pojos;

import java.time.LocalDate;
import java.util.UUID;


/**
 * This is an entity class for defining details of a forecast
 */

//@Entity
//@Table(name = "Forecast")
public class Forecast {

	/**
	 * Primary key for this entity and is uniquely generated
	 */

//	@Id
//	@GeneratedValue
//	@Column(name = "forecastid", nullable = false)
	private UUID forecastID;
//	@Column(name = "forecast_name", nullable = false, unique = true)
	private String forecastName;
//	@Column(name = "forecast_description", nullable = false)
	private String forecastDescription;
//	@Column(name = "visibility", nullable = false)
	private String visibility;
//	@Column(name = "startDate", nullable = false)
	private java.time.LocalDate startDate;
//	@Column(name = "endDate", nullable = false)
	private java.time.LocalDate endDate;
//	@Column(name = "horizon_period", nullable = false)
	private int horizonPeriod;
//	@Column(name = "created_by", nullable = false)
	private String createdBy;
//	@Column(name = "created_time", nullable = false)
	private java.time.LocalDate createdTime;
//	@Column(name = "updated_by", nullable = false)
	private String updatedBy;
//	@Column(name = "updated_time", nullable = false)
	private java.time.LocalDate updatedTime;

	/**
	 * Foreign keys for this entity referencing the primary key of other tables
	 */

//	@ManyToOne()
//	@JoinColumn(name = "accountID", nullable = false)
	private AccountDetails accountID;

//	@ManyToOne
//	@JoinColumn(name = "roleID", nullable = false)
	private UserRole roleID;

	public Forecast() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Forecast(UUID forecastID, String forecastName, String forecastDescription, String visibility,
			int horizonPeriod, String createdBy, String updatedBy, AccountDetails accountID, UserRole roleID) {
		super();
		this.forecastID = forecastID;
		this.forecastName = forecastName;
		this.forecastDescription = forecastDescription;
		this.visibility = visibility;
//		this.startDate = startDate;
//		this.endDate = endDate;
		this.horizonPeriod = horizonPeriod;
		this.createdBy = createdBy;
//		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
//		this.updatedTime = updatedTime;
		this.accountID = accountID;
		this.roleID = roleID;
	}

	/**
	 * Getters and Setters for this class
	 * 
	 */

	public UUID getForecastID() {
		return forecastID;
	}

	public void setForecastID(UUID forecastID) {
		this.forecastID = forecastID;
	}

	public String getForecastName() {
		return forecastName;
	}

	public void setForecastName(String forecastName) {
		this.forecastName = forecastName;
	}

	public String getForecastDescription() {
		return forecastDescription;
	}

	public void setForecastDescription(String forecastDescription) {
		this.forecastDescription = forecastDescription;
	}

	public String getVisibility() {
		return visibility;
	}

	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	public int getHorizonPeriod() {
		return horizonPeriod;
	}

	public void setHorizonPeriod(int horizonPeriod) {
		this.horizonPeriod = horizonPeriod;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDate getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(LocalDate createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public LocalDate getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(LocalDate updatedTime) {
		this.updatedTime = updatedTime;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public AccountDetails getAccountID() {
		return accountID;
	}

	public void setAccountID(AccountDetails accountID) {
		this.accountID = accountID;
	}

	public UserRole getRoleID() {
		return roleID;
	}

	public void setRoleID(UserRole roleID) {
		this.roleID = roleID;
	}

	@Override
	public String toString() {
		return "Forecast [forecastID=" + forecastID + ", forecastName=" + forecastName + ", forecastDescription="
				+ forecastDescription + ", visibility=" + visibility + ", startDate=" + startDate + ", endDate="
				+ endDate + ", horizonPeriod=" + horizonPeriod + ", createdBy=" + createdBy + ", createdTime="
				+ createdTime + ", updatedBy=" + updatedBy + ", updatedTime=" + updatedTime + ", accountID=" + accountID
				+ ", roleID=" + roleID + "]";
	}

}

